import time
import random
import pyautogui


def move_mouse():
    screen_size = pyautogui.size()
    x, y = pyautogui.position()
    x += 1
    y += 1
    duration = random.uniform(0.1, 0.2)
    print(f"X: {x}")
    print(f"Y: {y}")
    print(f"Duration: {duration}")
    print("Moving mouse!")
    pyautogui.moveTo(x, y, duration)
    print("Waiting 180 seconds")
    time.sleep(180)
    move_mouse()


def main():
    pyautogui.FAILSAFE = False
    move_mouse()


if __name__ == '__main__':
    main()
